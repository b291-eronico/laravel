@extends('layouts.app')

@section('content')
    <h2>{{$post->title}}</h2>

    <div class="card mb-3">
        <div class="card-body">
            <p class="card-text">{{$post->content}}</p>
            <p class="card-text"><small class="text-muted">Created at: {{$post->created_at}}</small></p>
        </div>
    </div>

    <h3>Comments:</h3>

    @if(count($comments) > 0)
        @foreach($comments as $comment)
            <div class="card mb-3">
                <div class="card-body">
                    <p class="card-text">{{$comment->content}}</p>
                    <p class="card-text"><small class="text-muted">Posted by {{$comment->user->name}} at {{$comment->created_at}}</small></p>
                </div>
            </div>
        @endforeach
    @else
        <p>No comments yet.</p>
    @endif

    <form method="POST" action="/posts/{{$post->id}}/comments">
        @csrf
        <div class="form-group">
            <label for="content">Add a comment:</label>
            <textarea class="form-control" id="content" name="content" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
