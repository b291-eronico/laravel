@extends('layouts.app')


@section('content')

		<div class="card">
			<div class="card-body">
				<h2 class="card-title">{{$post->title}}</h2>
				<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
				<p class="card-subtitle text-muted mb-3">Created at {{$post->created_at}}</p>
				<p class="card-text">{{$post->content}}</p>

				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						
						@method('PUT')
						@csrf

						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif
						
					</form>

					<form action="/posts/{{$post->id}}" method="POST">
						@csrf
						<input type="hidden" name="post_id" value="{{ $post->id }}">
						<div class="form-group">
							<label for="comment">Add Comment:</label>
							<textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				@endif

				<div class="mt-3">
					<a href="/posts" class="card-link">View all posts</a>	
				</div>
			</div>
			@if(!empty($comments))
				<div class="card mt-3">
					<div class="card-body">
						<h3 class="card-title">Comments:</h3>
						@foreach ($comments as $comment)
							<div class="card mt-3">
								<div class="card-body">
									<p class="card-text">{{ $comment->comment}}</p>
									<p class="card-subtitle text-muted">By {{ $comment->user->name }} on {{ $comment->created_at }}</p>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			@endif
		</div>

@endsection
